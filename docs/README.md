[![wakatime](https://wakatime.com/badge/github/Ayusummer/DailyNotes.svg)](https://wakatime.com/badge/github/Ayusummer/DailyNotes)

# 概述

233 的日常学习记录随笔

- 在线阅读
  - [在线阅读-Gitlab](https://ayusummer233.gitlab.io/dailynotesbackup/)
  - [在线阅读-GHithub](https://ayusummer.github.io/DailyNotes/)

- 源码
  - 文档
    - [文档-Gitlab](https://gitlab.com/ayusummer233/dailynotesbackup)
    - [文档-Github](https://github.com/Ayusummer/DailyNotes)

  - 代码
    - [代码-Gitlab](https://gitlab.com/ayusummer233/dailynotescodebackup)
    - [代码-Github](https://github.com/Ayusummer/DailyNotesCode)


---
