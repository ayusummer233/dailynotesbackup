# Linux

- [Linux](#linux)
  - [Ubuntu 的 source.list 文件](#ubuntu-的-sourcelist-文件)
    - [换源](#换源)
  - [SSH](#ssh)
    - [VSCode: Remote-SSH](#vscode-remote-ssh)
    - [MobaXterm](#mobaxterm)
    - [WindTerm](#windterm)
    - [Terminus](#terminus)
    - [远程图形化界面的本地显示](#远程图形化界面的本地显示)
      - [一些软件命令行启动的命令](#一些软件命令行启动的命令)
    - [使用 SSH 做端口转发让服务器用本地的 clash 代理](#使用-ssh-做端口转发让服务器用本地的-clash-代理)
  - [根目录各目录含义](#根目录各目录含义)
  - [SHELL](#shell)
    - [类清屏](#类清屏)
- [清屏](#清屏)
- [指针移到行尾](#指针移到行尾)
    - [手动校准时间](#手动校准时间)
    - [用户管理](#用户管理)
    - [echo](#echo)
    - [防火墙相关](#防火墙相关)
    - [Cron 表达式](#cron-表达式)
      - [各字段含义](#各字段含义)
      - [常用 Cron 表达式](#常用-cron-表达式)
  - [文件操作](#文件操作)
    - [计算文件占用空间](#计算文件占用空间)
    - [查找文件](#查找文件)
    - [计算目录下的文件数量](#计算目录下的文件数量)
  - [压缩与解压](#压缩与解压)
    - [zip](#zip)
    - [gz](#gz)
    - [tar.gz](#targz)
    - [7z](#7z)
  - [使用 root 登入 UI](#使用-root-登入-ui)
  - [软硬链接](#软硬链接)
  - [网络](#网络)
    - [路由操作](#路由操作)
      - [ip route](#ip-route)
      - [route(deprecated)](#routedeprecated)
    - [NetworkManager](#networkmanager)
      - [默认路由](#默认路由)
    - [启用与禁用网卡](#启用与禁用网卡)
    - [IP 转换](#ip-转换)
    - [防火墙](#防火墙)
      - [iptables](#iptables)
        - [添加规则](#添加规则)
    - [代理](#代理)
  - [WSL2](#wsl2)
    - [安装](#安装)
    - [卸载](#卸载)
    - [VSCode-ssh-remote](#vscode-ssh-remote)
    - [端口映射](#端口映射)
    - [WSL2 DNS 服务异常](#wsl2-dns-服务异常)
    - [报错收集](#报错收集)
      - [ssh 拒绝](#ssh-拒绝)
      - [ping 的通 ip , ping 不通域名](#ping-的通-ip--ping-不通域名)
  - [服务器](#服务器)
    - [远程连接服务器](#远程连接服务器)
      - [remote-SSH](#remote-ssh)
    - [文件下载](#文件下载)
    - [腾讯云轻量](#腾讯云轻量)
      - [内网 DNS](#内网-dns)
      - [使用密钥登录到 root 账户](#使用密钥登录到-root-账户)
    - [探针](#探针)
  - [窗口工具](#窗口工具)
    - [Zellij](#zellij)
      - [安装](#安装-1)
      - [使用](#使用)
    - [Screen 命令](#screen-命令)
      - [语法](#语法)
  - [软件](#软件)
    - [命令行安装](#命令行安装)
    - [查看软件安装位置](#查看软件安装位置)
    - [软件包](#软件包)
    - [Firefox](#firefox)
    - [微信](#微信)
  - [常见问题](#常见问题)
    - [the root filesystem require a manual fsck](#the-root-filesystem-require-a-manual-fsck)
    - [E: dpkg was interrupted, you must manually run 'dpkg --configure -a' to correct the problem.](#e-dpkg-was-interrupted-you-must-manually-run-dpkg---configure--a-to-correct-the-problem)
    - [E: Sub-process /usr/bin/dpkg returned an error code (1)](#e-sub-process-usrbindpkg-returned-an-error-code-1)
  - [game](#game)
    - [手游相关](#手游相关)
  - [Ubuntu 安装邮件服务器(TODO - 校验有问题且暂时不打算用, 已搁置)](#ubuntu-安装邮件服务器todo---校验有问题且暂时不打算用-已搁置)

---

## Ubuntu 的 source.list 文件

> [Ubuntu | 对 sources.list 的总结 - 简书 (jianshu.com)](https://www.jianshu.com/p/5400722c369c)
>
> [详解 Ubuntu 的 source.list 文件\_VinQin 的博客-CSDN 博客\_sourcelist](https://blog.csdn.net/u012843189/article/details/80964287)

### 换源

> [vim - Ubuntu 20.04 Desktop 换源的两种方法\_个人文章 - SegmentFault 思否](https://segmentfault.com/a/1190000040946515)

换网易源:

打开 `/etc/apt/sources.list` 在文件首部加上如下配置

```
# 镜像
deb http://mirrors.163.com/ubuntu/ focal main restricted
deb http://mirrors.163.com/ubuntu/ focal universe
deb http://mirrors.163.com/ubuntu/ focal multiverse
deb http://mirrors.163.com/ubuntu/ focal-updates main restricted
deb http://mirrors.163.com/ubuntu/ focal-updates universe
deb http://mirrors.163.com/ubuntu/ focal-updates multiverse
deb http://mirrors.163.com/ubuntu/ focal-backports main restricted universe multiverse
```

然后注释掉相应后缀的源本的官方源之后更新下索引即可

```bash
apt update
```

---


## SSH

> [如何在 Ubuntu 20.04 启用 SSH-阿里云开发者社区 (aliyun.com)](https://developer.aliyun.com/article/763505)
>
> ---

首先需要确认自己的机子是否有 SSH 服务, 如果 SSH 不能连上本机的话那么需要装下 openssh

```bash
# 刚装好系统需要配置下 root 密码, 输入如下命令然后输入当前账户密码后设置 root 密码即可
sudo passwd root

# 更新源
apt update
# 安装 openssh-server
apt install openssh-server
```

安装完成后 SSH 服务会自动启动

```bash
# 验证 SSH 是否在运行
systemctl status ssh
```

> 按 `q` 返回命令行

需要注意的是 ubuntu 自带一个配置 iptables 防火墙的工具 UFW(`Uncomplicated Firewall`), 如果系统防火墙已经启用那么请确保打开了 SSH 端口

```bash
ufw allow ssh
```

到此为止就可以使用普通账户 ssh 登录了, 但是还不能用 root 来 ssh 连接, 还需要再配置下

```bash
# 安装 vim
apt install vim

# 打开 sshd_config 文件
vim /etc/ssh/sshd_config
# 按下 i 切换到编辑模式进行文本编辑
# 编辑完成后 esc 后输入 :wq 并回车即可保存并退出 vim
```

将 `#Authentication` 项目下的 `PermitRootLogin` 设置为 `yes`, `PasswordAuthentication` 项也设置为 `yes`

> 如果后者没有就新建一个

> ![image-20221110002027202](http://cdn.ayusummer233.top/img/202211100020293.png)  
> ![image-20221110002039422](http://cdn.ayusummer233.top/img/202211100020468.png)

```bash
# 重启 ssh 服务
service ssh restart
# 添加开机启动
update-rc.d ssh enable
```

然后就可以使用 root 账户 ssh 该设备了

---

### VSCode: Remote-SSH

VSCode 安装 Remote-SSH

> ![image-20221110003106144](http://cdn.ayusummer233.top/img/202211100031215.png)

打开 Remote-SSH 配置项

> ![image-20221110003320756](http://cdn.ayusummer233.top/img/202211100033780.png)

填入

```properties
Host [为该主机随便起个有辨识度的名字]
    HostName [主机ip]
    User [登入用户, 可以填 root]
```

连接到远程然后根据提示选择 Linux, 输入密码即可

---

在本地打开命令行执行生成密钥命令:

```bash
ssh-keygen -t rsa -C "youremail@example.com"
```

- ``-C(comment) `随便填, 有辨识度就行

根据提示完成密钥生成步骤(可以什么都不输入一路回车到完成)

完成后会生成一个私钥(`id_rsa`)一个公钥(`id_rsa_pub`)

将**本地公钥**复制到远程主机的 `/root/.ssh` 目录下然后在终端中 cd 到该目录执行(如果该目录不存在则先创建此目录)

```bash
cat id_rsa_ubuntu1.pub >> authorized_keys
sudo chmod 600 authorized_keys	# 修改文件权限
sudo chmod 700 ~/.ssh	# 修改目录权限
```

然后打开 remote-ssh 配置文件, 在原来配置项的基础上加上一个 `IdentityFile` 字段, 填写上==本地私钥==路径即可

```properties
Host [为该主机随便起个有辨识度的名字]
    HostName [主机ip]
    User [root]
    IdentityFile "[本地私钥路径]"
```

然后重新连接远程主机, 就不需要输入密码了

---

### MobaXterm

> [【MobaXterm】设置保持 SSH 连接\_hitrjj 的博客-CSDN 博客\_mobaeterm keepalive](https://blog.csdn.net/u014636245/article/details/83855860)

---

### WindTerm

---

### Terminus

---

### 远程图形化界面的本地显示

> [ssh 链接远程服务器 及 远程图形化界面的本地显示 - 掘金 (juejin.cn)](https://juejin.cn/post/7109647016086470669)
>
> [本地显示远程图形化界面、服务器配置图形化界面 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/260189540)
>
> [Installing and running an X Server on Windows — Aalto scientific computing](https://scicomp.aalto.fi/triton/quickstart/installxonwindows/)
>
> ---

编辑 `/etc/ssh/sshd_config`

![image-20221201165743295](http://cdn.ayusummer233.top/img/202212011718395.png)

修改如下三条配置:

```properties
X11Forwarding yes
#X11DisplayOffset 10
X11UseLocalhost no
#PermitTTY yes
PrintMotd no
#PrintLastLog yes
#TCPKeepAlive yes
```

```bash
# 重启 ssh
service ssh reload
# 安装 x11-apps
sudo apt install x11-apps
```

到这里 MobaXterm 就可以在本地显示远程应用的 UI 了

![image-20221201170353587](http://cdn.ayusummer233.top/img/202212011718703.png)

但是 VSCode 没有 DISPLAY 环境变量, 需要在 MobaXterm 里执行下

```bash
env | grep DISPLAY
```

> ![image-20221201170530202](http://cdn.ayusummer233.top/img/202212011718613.png)

对应得将如下配置添加到 `/root/.bashrc` 中:

```properties
export DISPLAY=localhost:11.0
```

![image-20221201170804190](http://cdn.ayusummer233.top/img/202212011718410.png)

> PS: 这个 IDSPLAY 变量的值是会变的, 貌似是每次 MobaXterm SSH 连接设备都会变
>
> ![image-20221202133848680](http://cdn.ayusummer233.top/img/202212021823466.png)
>
> > [xorg - What is the $DISPLAY environment variable? - Ask Ubuntu](https://askubuntu.com/questions/432255/what-is-the-display-environment-variable)
> >
> > [使用 WSL2 + X11 转发 - 在 Windows10 中打造 GNU/Linux 学习生产环境 - Steins;Lab (steinslab.io)](https://steinslab.io/archives/2082#3_X11_Forwarding)
>
> 折腾了一圈最后感觉还是开个 MobaXterm 然后用 VSCode 比较方便

---

#### 一些软件命令行启动的命令

```bash
# 火狐浏览器直接在命令行里输入 firefox 并回车会在远程启动默认用户配置的 Firefox 窗口, 并不会在本地启动
firefox
# 如果要在本地启动的话需要用如下配置调起火狐用户配置, 然后新建一个用户配置并启动, 此时在本地就可以看到火狐的窗口了
firefox -profilemanager
```

![image-20221201191455296](http://cdn.ayusummer233.top/img/202212021823418.png)

> 不过远程启动火狐后使用体验不是很好, 比较卡, 找到的一篇相关文章也并没有复现成功, 于是就继续远程 windows 用浏览器了
>
> [为什么 Firefox 在 SSH 上这么慢？ - rebeca8 - 博客园 (cnblogs.com)](https://www.cnblogs.com/zafu/p/9392498.html)
>
> ---
>
> 从个人实际需求出发之后发现了一个比较好的替代方案
>
> 因为个人希望打开远程浏览器主要是为了访问局域网里的靶场, 然后通过 burp 拦截请求
>
> 那么可以用 VSCode 的端口转发功能, 将 BurpSuit 代理的端口(比如 8080) 转发到本机, 然后在本机的 firefox 设置 localhost 8080 代理, 之后就可以在本机 firefox 中访问局域网靶场以及使用 burp 拦截请求了
>
> > PS: 单独设置 VSCode 的端口转发以及 FireFox 的代理并不能使 Firefox 访问局域网站点, 需要用 burp 也代理相同端口才能正常访问局域网站点
>
> ![image-20221202183951903](http://cdn.ayusummer233.top/img/202212021839812.png)
>
> ![image-20221202184032013](http://cdn.ayusummer233.top/img/202212021907976.png)
>
> ![image-20221202184101762](http://cdn.ayusummer233.top/img/202212021907666.png)
>
> ![image-20221202190638055](http://cdn.ayusummer233.top/img/202212021908693.png)
>
> ![image-20221202190726334](http://cdn.ayusummer233.top/img/202212021908052.png)

---

```bash
java -jar [burpsuitxxx.jar绝对路径]
```

> ![image-20221202093238590](http://cdn.ayusummer233.top/img/202212021823179.png)
>
> 就是分辨率有点奇怪, 可以在 `~/.bashrc` 加上 `GDK_SCALE` 参数来放大 [GDK_SCALE] 倍(只能是整数倍)
>
> ```bash
> export GDK_SCALE=2
> export GDK_DPI_SCALE=1
> ```

---

### 使用 SSH 做端口转发让服务器用本地的 clash 代理

首先在本地将 Clash 的 `Allow Lan` 打开

使用 SSH 创建端口转发

```bash
ssh -fNR 7890:localhost:7890 -i [ssh私钥绝对路径] [用户名]@[服务器IP]
```

- `-f` 后台运行
- `-N` 不执行远程命令, 仅做端口转发
- `-R` 远程端口转发

如此一来就可以在服务器上使用本地的 Clash 代理了

- `http代理`: `http://localhost:7890`
- `socks5代理`: `socks5://localhost:7890`


---

## 根目录各目录含义

![image-20230920150853543](http://cdn.ayusummer233.top/DailyNotes/202309201508290.png)

- **/bin**：二进制文件的存储位置。包含了系统启动和修复所需的基本命令，如ls、cp、mv等。
- **/boot**：包含启动Ubuntu Linux所需的内核文件和引导加载程序配置文件。
- **/dev**：设备文件目录。包含系统用于与硬件设备进行通信的特殊文件，如磁盘分区、USB设备、键盘等。
- **/etc**：配置文件的存储位置。包含系统和应用程序的配置文件，用于管理系统和应用程序的设置。
- **/home**：用户的主目录。每个用户通常都有一个子目录，用于存储其个人文件和设置。
- **/lib**：共享库文件的存储位置, 存放着系统最基本的动态链接共享库，类似于 Windows 里的 DLL 文件。几乎所有的应用程序都需要用到这些共享库,包含了用于系统启动和运行的共享库。
- **/media**：可移动媒体设备的挂载点。当插入USB闪存驱动器或CD/DVD时，这些设备通常会在此处挂载。
- **/mnt**：手动挂载其他文件系统的临时挂载点。
- **/opt**：可选软件包的安装位置。某些第三方软件可能安装在此处。
- **/proc**：虚拟文件系统，用于访问有关系统进程和内核状态的信息。
- **/root**：超级用户(root) 的主目录。
- **/run**：在系统启动期间创建的临时运行时文件的存储位置。
- **/sbin**：系统命令的存储位置。包含只能由超级用户执行的系统命令。
- **/srv**：服务数据的存储位置。用于存储系统提供的一些服务的数据。
- **/sys**：用于与Linux内核进行交互的虚拟文件系统。
- **/tmp**：临时文件的存储位置。通常用于存储临时数据，文件在重启后会被清除。
- **/usr**：用户数据的次要存储位置。包含系统的大多数用户级程序和文件，包括可执行文件、库文件、头文件等。
- **/var**：可变数据的存储位置。包括日志文件、数据库文件、邮件和其他可变数据。


---

## SHELL

> [Bash 编程入门-1：Shell 与 Bash - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/56532223)

shell 是运行在终端中的文本互动程序，bash(GNU Bourne-Again Shell) 是最常用的一种 shell。是当前大多数 Linux 发行版的默认 Shell。

其他的 shell 还有：sh、bash、ksh、rsh、csh 等。Ubuntu 系统常用的是 bash，Bio-linux 系统是基于 ubuntu 定制的，但是却使用了 zsh。

sh 的全名是 Bourne Shell。名字中的玻恩就是这个 Shell 的作者。

而 bash 的全名是 Bourne Again Shell。最开始在 Unix 系统中流行的是 sh，而 bash 作为 sh 的改进版本，提供了更加丰富的功能。一般来说，都推荐使用 bash 作为默认的 Shell。

查看当前系统中 shell 的类型:

```shell
echo $SHELL
```

![20211219065045](http://cdn.ayusummer233.top/img/20211219065045.png)

---

### 类清屏

- ```bash
  # 清屏
  clear
  # 指针移到行尾
  Ctrl+L
  ```

---

### 历史记录

> [谁动了我的 Linux？原来 history 可以这么强大！ - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/371739269)

使用 history 命令可以查看当前用户执行的历史命令

```bash
history
```

![image-20230725143721312](http://cdn.ayusummer233.top/DailyNotes/202307251437125.png)

此外, 每个用户根目录下还都有一个 `.bash_history` 文件, 也存储了 bash 历史记录:

![image-20230725144046356](http://cdn.ayusummer233.top/DailyNotes/202307251500784.png)

不过这样看到的历史命令没有时间的, 需要时间的话还需要

```bash
export HISTTIMEFORMAT="%Y-%m-%d %T"
```

或者写到 `/root/.bashrc` 中然后 `source /root/.bashrc`

```bash
# 先写个空字符加换行进去
echo '' >> /root/.bashrc
echo 'export HISTTIMEFORMAT="%Y-%m-%d %T"' >> /root/.bashrc
source /root/.bashrc
```

这样再 history 就能看到带时间的日志了, 不过稍早一些的日志已经无可考证时间了, 毕竟当时执行的时候没保存时间戳

![image-20230725150042919](http://cdn.ayusummer233.top/DailyNotes/202307251500923.png)

此外 `.bash_history` 并非实时操作的, 正常退出 shell (`Ctrl+D`, `exit`)时, shell 进程会把历史记录缓冲区的内容写到 `.bash_history` 中

---

### 手动校准时间

```bash
# 修改日期
sudo date +%Y%m%d -s "20240119"
# 修改时间
sudo date +%T -s "11:20:00"
# 修改日期时间
sudo date +%Y%m%d%H%M.%S -s "202401191120.00"
```

![image-20240119112242251](http://cdn.ayusummer233.top/DailyNotes/202401191124984.png)

---

### 用户管理

```bash
# 查看当前登录用户
who
# 查看最近登录的用户
last
# 查看所用用户
cat /etc/passwd
```

- 新建用户

  ```bash
  sudo adduser newusername
  ```

---

### echo

> [How to use Echo Command in Linux (With Examples) (phoenixnap.com)](https://phoenixnap.com/kb/echo-command-linux)

```bash
# 帮助文档
/bin/echo --help
```

```bash
# 语法
echo [option] [string]
```



---

### 防火墙相关

> [Debian/Ubuntu/Centos 防火墙放行指定端口 - SunPma'Blog](https://sunpma.com/555.html)
>
> [ubuntu 的 ufw 如何开放特定端口?\_justheretobe 的博客-CSDN 博客\_ufw 开放端口](https://blog.csdn.net/justheretobe/article/details/51843178)

---

### Cron 表达式

> [cron 表达式详解 - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1674682)

Cron 是类 Unix 操作系统中一个基于时间的工作调度器, Cron 表达式使用字符串标识, 定义了一个 Cron 工作的运行时间, 由 6 个或 7 个字段组成, 各字段按照先后顺序分别标识 `分钟  小时  月份中的天(1-31)  月份  星期几 年份(可选) `

例如如下表达式表示在每天 `0:00` 运行任务: `0 0 * * *`

---

#### 各字段含义

|       字段       |                允许值                 |  允许的特殊字符   |
| :--------------: | :-----------------------------------: | :---------------: |
|   秒(Seconds)    |              0~59 的整数              |     `, - * /`     |
|   分(Minutes)    |              0~59 的整数              |     `, - * /`     |
|   小时(Hours)    |              0~23 的整数              |     `, - * /`     |
| 日期(DayofMonth) | 1~31 的整数(但是你需要考虑你月的天数) | `,- * ? / L W C`  |
|   月份(Month)    |        1~12 的整数或者 JAN-DEC        |     `, - * /`     |
| 星期(DayofWeek)  |    1~7 的整数或者 SUN-SAT (1=SUN)     | `, - * ? / L C #` |
|  年(可选)(Year)  |               1970~2099               |     `, - * /`     |

> 由于调度作业通常不需要秒字段, 因此很多情况下 5 个字段的 cron 表达式就足够表示需要的时间了, 当一个 cron 表达式只有 5 个字段时, 其等效于秒字段为 0 其他字段与其相同的 cron 表达式

- `:` 匹配任意值, 即在每个当前域的每个时间单位都触发一次, 比如用在 `分` 内则表示每分钟触发一次

- `?` 只能用在 `日期(DayofMonth)` 和 `星期(DayofWeek) ` 两个域, 含义与 `*` 相似但不同, 比如

- `-` 表示范围, 如 `时` 字段为 `9-17` 表示 `[9时, 17时]`

- `/` 表示起始时间每隔固定时间触发一次, 比如 `时` 字段为 `9-17/2` 表示 `[9时, 17时]` 间每 2h 触发一次

- `,` 表示枚举, 比如 `时` 字段为 `9,17` 表示在 9 时与 17 时分别触发一次

- `L` 表示最后, 只能用在 `日期(DayofMonth)` 和 `星期(DayofWeek) ` 两个域; 如果在 DayofWeek 域使用 5L,意味着在最后的一个星期四触发。

- `W` 表示有效工作日(周一到周五),只能出现在 DayofMonth 域，系统将在离指定日期的最近的有效工作日触发事件。例如：在 DayofMonth 使用 5W

  - 如果 5 日是星期六，则将在最近的工作日：星期五，即 4 日触发。
  - 如果 5 日是星期天，则在 6 日(周一)触发；
  - 如果 5 日在星期一到星期五中的一天，则就在 5 日触发

  > - W 的最近寻找不会跨过月份
  > - LW 这两个字符可以连用，表示在某个月最后一个工作日，即最后一个星期五。

- `#` 用于确定每个月第几个星期几，只能出现在 DayofWeek 域。例如在 4#2，表示某月的第二个星期三

---

#### 常用 Cron 表达式

|                   含义                   |    Cron 表达式     |
| :--------------------------------------: | :----------------: |
|            周一到周五九点触发            |   `0 9 * * 1-5`    |
| 每个工作日的 9-19 点之间的每两个小时触发 | `0 9-19/2 * * 1-5` |
|                                          |                    |


---

## 文件操作


---

### 计算文件占用空间

```bash
# 计算当前目录及其子目录占用的空间(mb)
du -hsm .
```

- `du` 是一个用于统计目录或文件的磁盘使用情况的命令，它的全称是 disk usage。
- `-h` 是一个选项，它表示以人类可读的格式显示大小，例如 1K，234M，2G 等。
- `-s` 是一个选项，它表示只显示总和，而不显示每个子目录或文件的大小。
- `-m` 是一个选项，它表示以兆字节(MB) 为单位显示大小。
- `.` 是一个参数，它表示当前目录。

---


---

### 查找文件

- 使用 locate

  > [Difference between locate and mlocate - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/273182/difference-between-locate-and-mlocate)
  >
  > ***

  ```bash
  # 注意是 mlocate 而非 locate, 二者是不一样的
  apt install mlocate
  # 更新数据库
  time updatedb
  # 使用 mlocate 搜索文件(注意这里命令就是 locate 而非 mlocate)
  locate [文件名]
  ```

---

`使用 find` 命令

比如查找 success 文件

```bash
find / -name "success"
```

---

### 计算目录下的文件数量

查看当前目录下有多少文件

```bash
ls -l /path/to/directory | grep ^- | wc -l
```

- `ls -l` 列出指定目录中的所有文件和子目录的详细信息
- `grep ^-` 过滤出所有以`-`开头的行，这些行代表普通文件
- `wc -l` 统计行数，即文件的数量

---

查看当前目录及其子目录中有多少文件

```bash
find /path/to/directory -type f | wc -l
```


---

## 压缩与解压

### zip

```bash
# 解压 zip 文件
unzip [option] [压缩包名]
```

|   option    |                                             含义                                             |
| :---------: | :------------------------------------------------------------------------------------------: |
|  -d 目录名  |                                 将压缩文件解压到指定目录下。                                 |
|     -n      |                                解压时并不覆盖已经存在的文件。                                |
|     -o      |                         解压时覆盖已经存在的文件，并且无需用户确认。                         |
|     -v      | 查看压缩文件的详细信息，包括压缩文件中包含的文件大小、文件名以及压缩比等，但并不做解压操作。 |
|     -t      |                              测试压缩文件有无损坏，但并不解压。                              |
| -x 文件列表 |                           解压文件，但不包含文件列表中指定的文件。                           |

---

### gz

`.gz` 文件是使用 `gzip`(GNU zip) 压缩程序压缩的文件。

```bash
# 解压为 filename 并删除原始压缩文件, -d 可选
gunzip filename.gz
gzip -d filename.gz
# 使用  -k(keep) 以保留原始文件
gunzip -k filename.gz
# 查看压缩文件内容而不解压
zcat filename.gz
```

---

### tar.gz

`.tar.gz` 文件是一种在 Unix 和 Linux 系统中常见的压缩文件格式，它实际上结合了两种不同的技术：`tar` 和 `gzip`。

- **Tar(磁带归档) **：
  - `tar` 是一个用于打包多个文件和目录到单个文件(即归档文件) 的工具。这个过程不涉及压缩，仅仅是将多个文件合并成一个大文件，以便于管理和传输。
  - 由 `tar` 创建的文件通常有 `.tar` 扩展名。
- **Gzip(GNU zip) **：
  - `gzip` 是一个广泛使用的数据压缩程序，它使用 DEFLATE 压缩算法来减小文件大小。
  - `gzip` 通常用于压缩单个文件。压缩后的文件具有 `.gz` 扩展名。
- **结合 Tar 和 Gzip**：
  - 当需要压缩整个目录或多个文件时，首先使用 `tar` 将它们打包成一个 `.tar` 文件，然后使用 `gzip` 压缩这个 `.tar` 文件，生成 `.tar.gz` 或 `.tgz` 文件。
  - 这样做的好处是可以同时实现多个文件的打包和压缩，非常适用于备份、软件分发、日志文件的存储等场景。

```bash
# 创建 .tar.gz 文件
tar -czvf archive.tar.gz /path/to/directory
# 解压 .tar.gz 文件
tar -xzvf archive.tar.gz
```

- `c`: 创建归档文件
- `x`: 解压归档文件
- `z`: 使用 gzip 压缩/解压缩
- `v`: 显示详细信息
- `f`: 指定归档文件名


----

### 7z

如果没安装 `p7zip-full` 的话可以使用如下命令安装

```bash
sudo apt update
sudo apt install p7zip-full
```

安装完成后，使用以下命令解压 .7z 文件：

```bash
7z x file.7z
```

- `x`: 用于保持原目录结构的方式解压文件。

如果只想查看压缩包中的内容而不解压，可以使用以下命令：

```bash
7z l file.7z
```


---


---

## 使用 root 登入 UI

> [ubuntu20.04 使用 root 用户登录系统\_COCO56(徐可可) 的博客-CSDN 博客\_ubuntu 使用 root 登录](https://blog.csdn.net/COCO56/article/details/107628019)
>
> ---

> 不建议使用特权用户登入系统(一键扬掉系统.jpg)
>
> > [为什么 sudo 存在？为什么不将特权系统访问作为用户权限处理？ | 码农俱乐部 - Golang 中国 - Go 语言中文社区 (mlog.club)](https://mlog.club/article/4094413)
>
> ---

首先设置好 root 密码, 然后改几个文件

- `/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf`

  在末尾加上

  ```properties
  # 手工输入登录系统的用户名和密码
  greeter-show-manual-login=true
  ```

- `/etc/pam.d/gdm-autologin`

  使用 # 注释第三行的限制 root 登录:

  ```properties
  # auth	required	pam_succeed_if.so user != root quiet_success
  ```

- `/etc/pam.d/gdm-password`

  使用 # 注释第 3 行限制 root 登录的配置项:

  ```properties
  # auth	required	pam_succeed_if.so user != root quiet_success
  ```

- `/root/.profile`

  使用 # 注释掉最后一行然后添加一行

  ```properties
  # mesg n 2> /dev/null || true
  tty -s&&mesg n || true
  ```

- 重启设备然后即可使用 root 账户登入 UI 界面

  ```bash
  reboot
  ```

  > 然后就会看到不推荐使用特权用户登入系统

---

## 软硬链接

> [软连接和硬链接区别 - matengfei - 博客园 (cnblogs.com)](https://www.cnblogs.com/matengfei123/p/12824422.html)

---

## 网络

---


### 路由操作

#### ip route

- 查看路由表

  ```bash
  ip route show
  ```

- 添加一条路由

  ```bash
  ip route add [目的网段/掩码] via [网关] dev [网卡]
  ip route add 192.0.2.0/24 via 10.0.0.1 dev eth0
  ```

- 删除一条路由

  ```bash
  ip route del [目标网段/掩码] via [网关] dev [网卡]
  ip route del 192.0.2.0/24 via 10.0.0.1 dev eth0
  ```

- 查看某个目的的路由信息

  ```bash
  ip route get 192.0.2.1
  ```

---

#### route(deprecated)

> `ip route` 命令是属于 iproute2 套件的一部分，这个套件在现代的 Linux 系统中已经成为了网络配置的标准工具。
> 相较于旧的 `route` 命令，`ip route` 提供了更多的功能并且在设计上更加灵活和强大。

```bash
route add -net [目的网段] netmask [掩码] gw [网关]
route del -net [目的网段] netmask [掩码] gw [网关]
```

这样加的路由是临时的, 每次重启都会掉路由, 可以通过在 `/root/.bashrc` 中写入如下命令

```bash
# 如果路由中没有到目的网段 [目的网段] 的路由则添加此条路由
if ! ip route | grep -q [目的网段]; then
    route add -net [目的网段] netmask [子网掩码] gw [网关ip]
fi
```

由于每次打开 bash 都会加载 `~/.bashrc`, 而 VSCode SSH 连远程主机一般第一件事就是新建一个 bash, 所以这样也可以变相解决手动加路由的困扰

> 不用 bash 的话也可以手动 source ~/.bashrc 来加载路由
>
> ---
>
> `-q` 参数使得 `ip route | grep [目的网段]` 命令不输出结果, 不使用 `-d` 的话每次新建 bash 都会看到该条命令的输出结果

---



----

### NetworkManager

> [NetworkManager - 维基百科，自由的百科全书 (wikipedia.org)](https://zh.wikipedia.org/wiki/NetworkManager)
>
> [NetworkManager - Arch Linux 中文维基 (archlinuxcn.org)](https://wiki.archlinuxcn.org/wiki/NetworkManager?rdfrom=https%3A%2F%2Fwiki.archlinux.org%2Findex.php%3Ftitle%3DNetworkManager_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)%26redirect%3Dno)

NetworkManager 是 RedHat 公司在 2004 年发起的项目, 目标是让 Linux 用户能够更容易地处理现代的网络需求

对于旧有的 `ifupdown` 和 `/etc/network/interfaces` 配置而言, NetworkManager 提供了一个用户友好的 GUI, 让用户能够更轻松地管理网络连接和配置

- 列出所有连接

  ```bash
  nmcli connection show
  ```

  ![image-20231011094043329](http://cdn.ayusummer233.top/DailyNotes/202310110940549.png)

- 查看特定连接的详细信息

  ```bash
  nmcli connection show "连接名称"
  ```

  ![image-20231011094222201](http://cdn.ayusummer233.top/DailyNotes/202310110942198.png)

- 修改连接属性

  ```bash
  nmcli connection modify "连接名称" 属性 值
  ```

- 启用/禁用连接

  ```bash
  nmcli connection up "连接名称"
  nmcli connection down "连接名称"
  ```

- 删除连接

  ```bash
  nmcli connection delete "连接名称"
  ```

---

除了用命令行操作连接配置外, NetworkManager 也可以通过修改文件并重新加载网络配置

NetworkManager 的配置文件通常存储在 `/etc/NetworkManager/system-connections` 目录下, 每个连接对应一个配置文件

![image-20231011103622777](http://cdn.ayusummer233.top/DailyNotes/202310111036902.png)

![image-20231011103648130](http://cdn.ayusummer233.top/DailyNotes/202310111036655.png)

修改完成后保存, 然后重启网络服务使修改生效:

```bash
sudo systemctl restart NetworkManager
```

---

#### 默认路由

可以在 `Advanced Network Configuration` 中配置指定网卡的 `IPv4 Settings` 的 `Routes`, 可以配置默认路由

![image-20240815154532641](http://cdn.ayusummer233.top/DailyNotes/202408151547573.png)

![image-20240815154551436](http://cdn.ayusummer233.top/DailyNotes/202408151547410.png)

![image-20240815154929855](http://cdn.ayusummer233.top/DailyNotes/202408151549131.png)

---

删除默认的网关路由 `0.0.0.0/0` 则需要编辑 `/etc/NetworkManager/system-connections` 中对应网卡信息的配置文件

添加

```properties
never-default=true
```

![image-20240815155307972](http://cdn.ayusummer233.top/DailyNotes/202408151553183.png)

- `never-default=true`：此配置指示NetworkManager不要将该连接作为默认网关，从而避免创建默认路由`0.0.0.0/0`

----

### 启用与禁用网卡

```bash
# 禁用 ensxx(下面两条命令均可)
ip link set ensxx down
ifconfig  ensxx down
# 启用 ensxx(下面两条命令均可)
ip link set ensxx up
ifconfig  ensxx up
```

需要注意的是禁用网卡后相应的路由也会掉, 重新启用后需要重配路由

---

### IP 转换

> [/proc/sys/net/ipv4/ip_forward - ailx10 - 博客园 (cnblogs.com)](https://www.cnblogs.com/ailx10/p/5535943.html)

IP 地址分公有地址和私有地址

- public address 是由 INIC(internet network information center)负责，这些 ip 地址分配给注册并向 INIC 提出申请的组织机构。通过它访问 internet

- private address 是属于非注册地址，专门为组织内部使用;

  private ip address 是不可能直接用来跟 WAN 通信的，要么利用帧来通信(FRE 帧中继，HDLC,PPP) ,要么需要路由的 NAT 功能把私有地址转换为一个公有 ip

选择一台电脑(有两个网卡或者用单网卡然后用软件虚拟多一个网卡) 充当网关，一个网卡(eth0)连接外网 ISP，另一网卡(eth1)连接内网(即局域网)。局域网内的 ip 地址都是私用地址，只能在内部使用，在公网上是不可见的，所以局域网电脑要上网必须修改 ip，这就是网关的工作。

- 工作原理：

  内网主机向公网发送数据包时，由于目的主机跟源主机不在同一网段，所以数据包暂时发往内网默认网关处理，而本网段的主机对此数据包不做任何回应。

  由于源主机 ip 是私有的，禁止在公网使用，所以必须将数据包的源发送地址修改成公网上的可用 ip，这就是网关收到数据包之后首先要做的工作--ip 转换。

  然后网关再把数据包发往目的主机。目的主机收到数据包之后，只认为这是网关发送的请求，并不知道内网主机的存在，也没必要知道，目的主机处理完请求，把回应信息发还给网关。网关收到后，将目的主机发还的数据包的目的 ip 地址修改为发出请求的内网主机的 ip 地址，并将其发给内网主机。这就是网关的第二个工作--数据包的路由转发。

  内网的主机只要查看数据包的目的 ip 与发送请求的源主机 ip 地址相同，就会回应，这就完成了一次请求。

出于安全考虑，Linux 系统默认是禁止数据包转发的。所谓转发即当主机拥有多于一块的网卡时，其中一块收到数据包，根据数据包的目的 ip 地址将包发往本机另一网卡，该网卡根据路由表继续发送数据包。这通常就是路由器所要实现的功能。
配置 Linux 系统的 ip 转发功能，首先保证硬件连通，然后打开系统的转发功能

```bash
less /proc/sys/net/ipv4/ip_forward
```

> [Linux less 命令 | 菜鸟教程 (runoob.com)](https://www.runoob.com/linux/linux-comm-less.html)
>
> less 与 more 类似，less 可以随意浏览文件，支持翻页和搜索，支持向上翻页和向下翻页。

该文件内容为 0，表示禁止数据包转发，1 表示允许，将其修改为 1。可使用命令

```bash
echo "1" > /proc/sys/net/ipv4/ip_forward
```

修改文件内容，重启网络服务或主机后效果不再。

若要其自动执行，可将命令 `echo "1" > /proc/sys/net/ipv4/ip_forward` 写入脚本 `/etc/rc.d/rc.local` 或者 在 `/etc/sysconfig/network` 脚本中添加 `FORWARD_IPV4="YES"`

---

### 防火墙

#### iptables

> [Linux iptables 命令 - sparkdev - 博客园 (cnblogs.com)](https://www.cnblogs.com/sparkdev/p/9340924.html)

iptables 是 Linux 管理员用来设置 IPv4 数据包过滤条件和 NAT 的命令行工具。iptables 工具运行在用户态，主要是设置各种规则。而 netfilter 则运行在内核态，执行那些设置好的规则。

---

##### 添加规则

我们可以通过规则来匹配数据包，具体的匹配条件包括 IP、网段、网络接口(interface)和传输协议(tcp、udp 等)。
添加规则的命令格式如下：

```bash
iptables [-AI chain] [-io interface] [-p 协议] [-s 来源 IP] [-d 目标 IP] -j [ACCEPT,DROP,REJECT,LOG]
```

`-A`：针对某个规则链添加一条规则，新添加的规则排在现有规则的后面。
`-I`：针对某个规则链插入一条规则，可以为新插入的规则指定在链中的序号。如果不指定序号，则新的规则会变成第一条规则。
`-i`：指定数据包进入的那个网络接口，比如 eth0、lo 等，需要与 INPUT 链配合使用。
`-o`: 指定传出数据包的那个网络接口，需要与 OUTPUT 链配合使用。
`-p`: 指定此规则适用于那种网络协议(常用的协议有 tcp、udp、icmp，all 指适用于所有的协议)。
`-s`：指定数据包的来源 IP/网段，可以指定单个 IP，如 192.168.1.100，也可以指定一个网段，如 192.168.1.0/24。还可以通过 ！表示非的意思，如 ! 192.168.1.0/24 表示除了 192.168.1.0/24 之外的数据包。
`-d`：指定数据包的目标 IP/网段，其它与 -s 选项相同。
`-j`：指定匹配成功后的行为，主要有 ACCEPT、DROP、REJECT 和 LOG。

----

### 代理

::: tabs

@tab:active shell

```bash
# shell 中临时设置(若需要永久设置则写到 ~/.bashrc 中即可)
export http_proxy=http://127.0.0.1:7890
export https_proxy=http://127.0.0.1:7890
```

![image-20230905104050714](http://cdn.ayusummer233.top/DailyNotes/202309051040864.png)

```bash
# 下掉 proxy:
unset http_proxy
unset https_proxy
```

![image-20230905105927377](http://cdn.ayusummer233.top/DailyNotes/202309051059445.png)

@tab proxychains

安装 `proxychains`

```bash
sudo apt install proxychains
```

![image-20230905102950537](http://cdn.ayusummer233.top/DailyNotes/202309051029806.png)

打开上述报错中提到的 Config File, 编辑 `[ProxyList]` 属性为需要配置的代理

```properties
[ProxyList]
# add proxy here ...
# meanwile
# defaults set to "tor"
socks5 	127.0.0.1 7890
```

![image-20230905103741065](http://cdn.ayusummer233.top/DailyNotes/202309051037143.png)

然后在需要使用代理的命令前加上 `proxychains` 即可使用

![image-20230905103831201](http://cdn.ayusummer233.top/DailyNotes/202309051038312.png)

:::

---

## WSL2

---

### 安装

> [安装 WSL | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/wsl/install)
>
> [旧版 WSL 的手动安装步骤 | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/wsl/install-manual#step-3---enable-virtual-machine-feature)
>
> [win10 WSL2 问题解决 WslRegisterDistribution failed with error: 0x800701bc_first_Dance 的博客-CSDN 博客](https://blog.csdn.net/qq_18625805/article/details/109732122)
>
> ---

安装 WSL 2 之前，必须启用“虚拟机平台”可选功能。 计算机需要[虚拟化功能](https://learn.microsoft.com/zh-cn/windows/wsl/troubleshooting#error-0x80370102-the-virtual-machine-could-not-be-started-because-a-required-feature-is-not-installed)才能使用此功能。

以管理员身份打开 PowerShell 并运行：

```powershell
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

**重新启动**计算机，以完成 WSL 安装并更新到 WSL 2。

---

下载 Linux 内核更新包并安装

- [适用于 x64 计算机的 WSL2 Linux 内核更新包](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)

  > 如果使用的是 ARM64 计算机，请下载 [ARM64 包](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_arm64.msi)。 如果不确定自己计算机的类型，请打开命令提示符或 PowerShell，并输入：`systeminfo | find "System Type"`。 **Caveat：** 在非英文版 Windows 上，你可能必须修改搜索文本，对“System Type”字符串进行翻译。 你可能还需要对引号进行转义来用于 find 命令。 例如，在德语版中使用 `systeminfo | find '"Systemtyp"'`。

---

`Windows+X` 选择以管理员模式打开 Powershell, 执行如下命令安装 wsl2

```powershell
# 该命令默认安装 wsl2
wsl --install
```

![image-20221120231039275](http://cdn.ayusummer233.top/img/202211202310305.png)

```powershell
wsl --install -d kali-linux
```

按照提示新建账户密码即可

---

### 卸载

> [WSL 发行版卸载 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/487091950)
>
> ---

```powershell
wslconfig /u kali-linux
```

---

### VSCode-ssh-remote

使用 SSH-remote 插件连上 WSL 后如果不是以 root 用户登入的话,会在一些系统目录(如 `/etc`, `/dev`, `/root` 等)被限制编辑与增删, 不过在用户目录(如 `/ubuntu`, `/mnt`)的权限是足够的

如果想要登入后可以编辑系统目录文件的话就要使用 `root` 用户登录, 但是 remote-ssh 虽然对于 `SSH Targets` 有配置文件可以编辑登入用户, 但是没有关于 `WSL Targets` 的配置, 那么这就需要在更高的层级编辑默认以 `root` 身份登入 `WSL`

> [Change vscode user in remote-WSL · Issue #3631 · microsoft/vscode-remote-release (github.com)](https://github.com/microsoft/vscode-remote-release/issues/3631)
>
> [Manage Linux Distributions - Change the default user for a distribution | Microsoft Docs](https://docs.microsoft.com/en-us/windows/wsl/wsl-config#change-the-default-user-for-a-distribution)
>
> [Ubuntu : 无法将“Ubuntu”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。请检查名称的拼写，如果包括路径，请确保路径 正确，然后再试一次。 - z_zhiro - 博客园 (cnblogs.com)](https://www.cnblogs.com/Hiro666/p/14119763.html)

- 首先查看下当前出问题的 `WSL Distribution` 版本  
  `win+x` 打开 `Windows Terminal`, 输入如下命令查看所有的 `WSL Distribution`:

  ```shell
  wsl --list --all
  ```

  ![image-20210921163044694](http://cdn.ayusummer233.top/img/202109211630793.png)

  其实在 `Remote-ssh - WSL Targets` 目录下就可以看到当前的 `WSL Distribution`

  ![image-20210921163322476](http://cdn.ayusummer233.top/img/202109211633562.png)

- 确认当前的 `WSL Distribution` 后在 `Windows Terminal` 中输入

  ```shell
  <DistributionName> config --default-user <Username>
  ```

  就可以将 `WSL Distribution` 为 `DistributionName` 的 `WSL` 的默认登录用户切换为 `Username`, 如:

  ![image-20210921163536793](http://cdn.ayusummer233.top/img/202109211635853.png)

  > 需要注意的是, 虽然看到的 `Distribution` 为 `Ubuntu-20.04`, 但是输入命令时要写成 `ubuntu2004`
  >
  > 相应的看到的是 `kali-linux`, 但是输入命令时要用 `kali`

- 再打开相应 `WSL` 时就可以看到用户已经切换到相应设置的用户了

  ![image-20210921163927558](http://cdn.ayusummer233.top/img/202109211639773.png)

  再用 VSCode-SSH-remote 连接 WSL 时可以看到登入用户已经切换成刚才配置的用户了, 当切换的是 root 用户时, 此时就可以使用 VSCode 新建及编辑系统目录下的文件了

  ![image-20210921164444924](http://cdn.ayusummer233.top/img/202109211644088.png)

  ***

### 端口映射

正常情况下直接从本机 telnet wsl2 的端口是不通的, 需要映射 wsl2 端口到本机

> [wsl2 设置端口映射\_压码路的博客-CSDN 博客\_wsl 端口映射](https://blog.csdn.net/keyiis_sh/article/details/113819244)

```powershell
# 获取 wsl ip 地址
wsl -- ifconfig eth0
```

> ![image-20220806160015420](http://cdn.ayusummer233.top/img/202208061600558.png)

```powershell
# 随便看看本机端口有没有占用(比如9225)
netstat -aon | findstr "9225"
```

> ![image-20220806160222828](http://cdn.ayusummer233.top/img/202208061602939.png)

```powershell
# 将ip地址的对应的端口映射到宿主win10对应的端口
# 需要管理员权限
# netsh interface portproxy add v4tov4 listenport=[win10端口] listenaddress=0.0.0.0 connectport=[虚拟机的端口] connectaddress=[虚拟机的ip]
netsh interface portproxy add v4tov4 listenport=9225 listenaddress=0.0.0.0 connectport=69 connectaddress=172.29.61.202
```

> ![image-20220806160340771](http://cdn.ayusummer233.top/img/202208061603880.png)

```powershell
# 检测是否设置成功
netsh interface portproxy show all
```

> ![image-20220806160442677](http://cdn.ayusummer233.top/img/202208061604773.png)
>
> ```powershell
> # 删除端口转发
> netsh interface portproxy delete v4tov4 listenport=9225 listenaddress=0.0.0.0
> ```

---

### WSL2 DNS 服务异常

无法正确解析域名, 直接 ping ip 可以 ping 通, 排查了一圈发现主网也 ping 不通

> 解决方案: [WSL 2 自定义安装目录和网络配置\_daihaoxin 的专栏-CSDN 博客\_wsl2 目录](https://blog.csdn.net/daihaoxin/article/details/115978662)

![20211218213224](http://cdn.ayusummer233.top/img/20211218213224.png)

- 网络: 172.22.0.0, 20 位掩码

配置主网防火墙入站规则

- 规则类型: 自定义
- 程序: 所有程序
- 协议和端口: 默认值不做改动
- 作用域: 此规则适用于哪些本地 IP 地址?: 下列 IP 地址 -> 添加 -> 此 ip 地址或子网: `172.22.0.0/20`
- 操作: 允许连接
- 配置文件: 全选
- 名称自定义

然后在 WSL2 里重新 ping 主网又能 ping 通了, DNS 也正常了, 可以 ping 同其他域名了

> 缺点在于计算机重启后 WSL2 主网地址可能会变(  
> 需要再配下防火墙  
> 挺秃然的, 没有完全搞清楚原理, 无法一劳永逸地解决这个问题

---

### 报错收集

> [WSL2 踩坑分享 – xiabee](https://xiabee.cn/coding/wsl2/)
>
> [WSL2 网络异常排查 [ping 不通、网络地址异常、缺少默认路由、被宿主机防火墙拦截\] - 简书 (jianshu.com)](https://www.jianshu.com/p/ba2cf239ebe0)

---

#### ssh 拒绝

`ssh: connect to host localhost port 22: Connection refused`

> [wsl 的 ssh server 无法启动 (ssh localhost 时报错 ssh: connect to host localhost port 22: Connection refused) \_hxc2101 的博客-CSDN 博客](https://blog.csdn.net/hxc2101/article/details/113617870)

打开 `/etc/ssh/sshd_config` 将监听地址 localhost 取消注释:

![image-20211026214222894](http://cdn.ayusummer233.top/img/202110262142078.png)

然后重启 `ssh 服务`

```shell
service ssh restart
```

**mark 下这句 ssh 服务重启指令**, ssh localhost 能够正常运行后如果 WSL2 关闭重启了再 `ssh localhost` 可能还会 `Connection refused`, 这时只要再 `service ssh restart` 然后 `ssh localhost` 就可以了

![image-20211026214857109](http://cdn.ayusummer233.top/img/202110262148965.png)

---

#### ping 的通 ip , ping 不通域名

dns 解析错误

修改 `/etc/resolv.conf` 文件

```conf
nameserver 8.8.8.8
```

---

## 服务器

### 远程连接服务器

---

#### remote-SSH

先在控制台生成并绑定密钥(本地密钥妥善保管), 然后再重置 `root` 密码

> ![20211122113415](http://cdn.ayusummer233.top/img/20211122113415.png) > ![20211122113543](http://cdn.ayusummer233.top/img/20211122113543.png)

> [轻量应用服务器 重置密码 - 操作指南 - 文档中心 - 腾讯云 (tencent.com)](https://cloud.tencent.com/document/product/1207/44575)

打开 VSCode Remote-SSH 插件配置项

```shell
Host Ubuntu
    HostName 公网ip
    User ubuntu
    IdentityFile "本地密钥路径"

Host CentOS
    HostName 公网ip
    User root
    IdentityFile "本地密钥路径"
```

- 腾讯云轻量的 ubuntu 默认禁用 root 用户名通过密码方式登录实例, 如需开启请参考 [Ubuntu 系统如何使用 root 用户登录实例？](https://cloud.tencent.com/document/product/1207/44569#ubuntu-.E7.B3.BB.E7.BB.9F.E5.A6.82.E4.BD.95.E4.BD.BF.E7.94.A8-root-.E7.94.A8.E6.88.B7.E7.99.BB.E5.BD.95.E5.AE.9E.E4.BE.8B.EF.BC.9F)
  - 腾讯云启用 root 密码登录后将 `remote-ssh` 配置项中对应 `User` 改为 `root` 后进行远程连接即可使用 `root 密码` 登录到服务器
  - `CentOS` 的话直接使用 `root` 和 `密钥` 的配置就可以自动登录到 `root 账户`
  - 由于`腾讯云(ubuntu)`绑定密钥默认绑定在 `ubuntu` 用户下, 因此腾讯云使用 `root + 密钥` 的形式登录 `root` 账户需要将密钥拷贝到 `root` 账户配置下即可:
    ```shell
    cat /home/ubuntu/.ssh/authorized_keys > /root/.ssh/authorized_keys
    ```
    > [腾讯云 密钥直接登录 root_Xav Pun 的博客-CSDN 博客](https://blog.csdn.net/weixin_39591031/article/details/118700963)
- `阿里云` 和 `UCLOUD` 默认是支持 `root +  密钥`登录的

> [每天一个 linux 命令(10) ：cat 命令 - peida - 博客园 (cnblogs.com)](https://www.cnblogs.com/peida/archive/2012/10/30/2746968.html)
>
> - 显示文件内容 `cat [filename]`
>
>   ![image-20211123110321948](http://cdn.ayusummer233.top/img/202111231103098.png)
>
> - 创建一个文件 `cat > [filename]`
>
>   ![image-20211123111154541](http://cdn.ayusummer233.top/img/202111231111636.png)
>
> - 将若干个文件合并为一个文件: `cat file1 file2 > file`
>
>   ![image-20211123111347216](http://cdn.ayusummer233.top/img/202111231113303.png)

---

### 文件下载

- `VSCode` 连接到服务器确实可以在左栏 `资源管理器` 处选择文件(夹)右键下载, 不过服务器带宽小的话很容易断连
- `Xshell + Xftp` 正版要付费且没必要为了下载个文件就多装一个软件专门做这件事
- 所以考虑直接使用 `Linux scp 命令` 进行下载

`scp` 命令无法识别 `Windows 目录`, 所以要采用一些方式来将 `Windows 目录` 转化成 `Linux 目录`,`WSL` 可以做到这点

`Windows + X` 打开 `Windows 终端`, 随便选择安装了的一个 `ubuntu 发行版` 进入后可以看到当前命令行所在目录 `/mnt/c/Users/233`, 对应 `Windows` 的 `C:/Users/233 目录`

![image-20211101103247697](http://cdn.ayusummer233.top/img/202111011032910.png)

然后使用如下命令将服务器文件下载到本地:

```shell
scp [user]@[ip]:[Linux 服务器上目标文件的路径] [指定下载到windows本地的路径]
```

![image-20211101104310152](http://cdn.ayusummer233.top/img/202111011043258.png)

![image-20211101104334687](http://cdn.ayusummer233.top/img/202111011043774.png)

下载文件夹:

```shell
scp -r [user]@[ip]:[Linux 服务器上目标文件的路径] [指定下载到windows本地的路径]
```

![image-20211101104510504](http://cdn.ayusummer233.top/img/202111011045617.png)

![image-20211101104630367](http://cdn.ayusummer233.top/img/202111011046461.png)

> [一说 git bash 可以](https://blog.csdn.net/fakerswe/article/details/103178542), 不过我拿 `git bash` 用 `ssh 命令` 连接服务器总是被拒绝连接

---

### 腾讯云轻量

---

[云产品首单秒杀*云服务器秒杀*云数据库秒杀 - 腾讯云 (tencent.com)](https://cloud.tencent.com/act/new?from=14615)[PS: 2C4G 轻量首年 74]

---

#### 内网 DNS

- yum 命令报错: `Could not resolve host: mirrors.tencentyun.com; Unknown error`

  [Could not resolve host: mirrors.tencentyun.com_user2025 的博客-CSDN 博客](https://blog.csdn.net/user2025/article/details/107733068)

  原因：腾讯云服务器内网 yum 源的域名 mirrors.tencentyun.com 需要有内网的 DNS 才能访问，但是实际情况下，我们会根据需要修改 DNS，为了使用腾讯云内网快速稳定的内网源，我们需要把 DNS 恢复为内网 DNS，下面为各地区服务器 DNS 地址
  解决办法：
  (1) 修改服务器的 DNS 配置文件：`/etc/resolv.conf` ，请参阅如下文档添加对应地区的内网 DNS 服务器

  [云服务器 内网服务 - 产品简介 - 文档中心 - 腾讯云 (tencent.com)](https://cloud.tencent.com/document/product/213/5225)

  > 我用的上海地域的轻量, 配上海或者上海金融的 DNS 都不对, 最后无奈重置实例才发现原来应该配最后一个所有地域的那个 DNS
  >
  > ![image-20210916203841882](http://cdn.ayusummer233.top/img/202109162038974.png)

  (2) 重启网络服务

  ```shell
  # 重启方式1：
  /etc/init.d/network restart
  #重启方式2：
  systemctl restart network
  ```

---

#### 使用密钥登录到 root 账户

> [腾讯云 密钥直接登录 root_Xav Pun 的博客-CSDN 博客](https://blog.csdn.net/weixin_39591031/article/details/118700963)

- 腾讯云的 `ubuntu` 系统, 生成密钥后绑定服务器默认会绑定在 `ubuntu` 用户下, 若要通过密钥登录到 `root` 用户则需要将 `ubuntu` 用户下的密钥复制到 `root` 用户下:
  ```sh
  cat /home/ubuntu/.ssh/authorized_keys > /root/.ssh/authorized_keys
  ```
  然后就可以使用密钥登录到 `root` 用户了

---

### 探针

> [cokemine/ServerStatus-Hotaru: 云探针、多服务器探针、云监控、多服务器云监控 (github.com)](https://github.com/CokeMine/ServerStatus-Hotaru)

在连不上 GitHub 时使用方式

> Coding 目前好像是需要登录才能下载, 仓库提供的默认脚本使用 coding 会拉不下来仓库, 所以还是用 github
>
> 将源仓库中的 github 相关链接换成了 GitHub Proxy 对应链接, 于是有了下文中的脚本

- 服务端

  ```bash
  # 源仓库的 shell(由于有时服务器不一定可以连上 github 所以修改了其中的部分链接便有了下面第二个自己修改的 shell)
  # wget https://cokemine.coding.net/p/hotarunet/d/ServerStatus-Hotaru/git/raw/master/status.sh
  wget https://cdn.ayusummer233.top/shell/status.sh
  ```

  ```bash
  bash status.sh s
  ```

  > - `选择 GitHub / Coding.net`: 保持默认(Github)(1)
  > - `选择监听端口`: 保持默认(35601) 或者自己填个未被使用且已放通的端口
  > - `自动部署`: 保持默认(y)
  > - `输入本机域名或ip`: 没有域名就直接输入本机 ip
  > - `输入 ServerStatus 服务端中网站要设置的 域名/IP 的端口`: 随便输个未被使用且已放通的端口, 这个端口用于访问 Web 页面
  >
  > ![image-20220913174443396](http://cdn.ayusummer233.top/img/202209131744795.png)
  >
  > ![image-20220913174525133](http://cdn.ayusummer233.top/img/202209131745321.png)
  >
  > ![image-20220913174857476](http://cdn.ayusummer233.top/img/202209131748700.png)
  >
  > ![image-20220913174920330](http://cdn.ayusummer233.top/img/202209131749559.png)

- 客户端

  首先在服务端添加一个节点配置, 用于与客户端配置对接

  ```bash
  bash status.sh s
  ```

  - 进入 7-服务端配置
  - 1 - 节点配置
  - 设置节点账密(自定义, 之后客户端通过此项配置进行连接)以及基本信息

> ![image-20220913175404031](http://cdn.ayusummer233.top/img/202209131754161.png)
>
> ![image-20220913175713057](http://cdn.ayusummer233.top/img/202209131757271.png)

在客户端进行相应配置(与服务端刚才设置的节点信息一致即可)

```bash
# 源仓库的 shell(由于有时服务器不一定可以连上 github 所以修改了其中的部分链接便有了下面第二个自己修改的 shell)
# wget https://cokemine.coding.net/p/hotarunet/d/ServerStatus-Hotaru/git/raw/master/status.sh
wget https://cdn.ayusummer233.top/shell/status.sh
bash status.sh c
```

> ![image-20220913175901782](http://cdn.ayusummer233.top/img/202209131759040.png)

---

若客户端为 windows 则需要手动用 Python 跑下

```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py # 若未安装pip
python get-pip.py
# 可以换下源, 不换也行
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple/
pip install psutil
# 修改 status-psutil.py(在主仓库的 clients 目录中)
# https://github.com/cokemine/ServerStatus-Hotaru/blob/master/clients/status-psutil.py
# 运行程序, 也可以将这句写成个 bat 文件然后双击运行
python status-psutil.py
```

> cmd 在快速编辑模式下运行命令时, 若用户鼠标点击到窗口区域可能会引起程序阻塞, 可以将其点掉
>
> ![image-20220928100730680](http://cdn.ayusummer233.top/img/202209281007350.png)
>
> > 快速编辑模式是一种很便捷的操作方式：左键选中，右键复制以及右键从剪贴板粘贴内容等  
> > 如果鼠标选中控制台界面上的内容，控制台就被阻塞了  
> > 在 Windows Server 2012 及 Windowns 8 以上，控制台窗口的程序默认是打开“快速编辑模式”的开关的。

---

## 窗口工具

### Zellij

> [Zellij](https://zellij.dev/)

#### 安装

> [Linux Ubuntu 添加环境变量\_FarryNiu 的博客-CSDN 博客\_ubuntu 添加环境变量](https://blog.csdn.net/qq_43474959/article/details/115028848)
>
> [Installation - Zellij User Guide](https://zellij.dev/documentation/installation.html)

先在 [Zellij](https://zellij.dev/) 下载好压缩包, 然后传到 Linux 文件系统中

> 位置选定在自己想要安装 zellij 位置

解压:

```shell
tar -xvf zellij-x86_64-unknown-linux-musl.tar.gz
```

![](http://cdn.ayusummer233.top/img/202205041947326.png)

添加执行权限:

```shell
chmod +x zellij
```

运行 `zellij`:

```shell
./zellij
```

将 `zellij` 所在目录添加到 `PATH` 变量中以在任何地方使用 `zellij`:

打开 `/root/.bashrc` 在末尾加上如下内容:

```sh
export PATH="/home/ubuntu/zellij:$PATH"
```

> 若已经有了其他的环境变量, 请使用 `:` 将此条拼接在前面
>
> ![image-20220824141913053](http://cdn.ayusummer233.top/img/202208241419154.png)

然后:

```shell
source ~/.bashrc
```

然后就可以在任意位置使用 `zellij` 命令来启用 `zellij` 了

---

#### 使用

新建一个`session`

```shell
zellij
```

新建一个 `Tab`: `ctrl + t, n`

重命名 `Tab`: `ctrl + t, r`

新建一个 `pane`: `ctrl + p, n`

重命名 `pane`: `Ctrl + p, c`

detach session: `ctrl + o, d`

关闭 session: `Ctrl + q`

界面底部有提示, 很友好:

![image-20220504210749149](http://cdn.ayusummer233.top/img/202205042107266.png)

显示 session 列表: `zellij list-sessions` 或者 `zellij ls`

![image-20220504210932759](http://cdn.ayusummer233.top/img/202205042109817.png)

返回某个 session: `zellij attach xxx` 或者 `zellij a xxx`

---

### Screen 命令

Linux screen 命令用于多重视窗管理程序。

screen 为多重视窗管理程序。此处所谓的视窗，是指一个全屏幕的文字模式画面。通常只有在使用 telnet 登入主机或是使用老式的终端机时，才有可能用到 screen 程序。

#### 语法

```
screen [-AmRvx -ls -wipe][-d <作业名称>][-h <行数>][-r <作业名称>][-s <shell>][-S <作业名称>]
```

**参数说明**：

- `-A` 　将所有的视窗都调整为目前终端机的大小。
- `-d<作业名称>` 　将指定的 screen 作业离线。
- `-h<行数>` 　指定视窗的缓冲区行数。
- `-m` 　即使目前已在作业中的 screen 作业，仍强制建立新的 screen 作业。
- `-r<作业名称>` ：　恢复离线的 screen 作业。
- `-R` 　先试图恢复离线的作业。若找不到离线的作业，即建立新的 screen 作业。
- `-s<shell>    <视窗名>` 　：指定建立新视窗时，所要执行的 shell。
- `-S<作业名称>` 　:指定 screen 作业的名称。
- `-v` 　显示版本信息。
- `-x` 　恢复之前离线的 screen 作业。
- `-ls或--list` 　显示目前所有的 screen 作业。
- `-wipe` 　检查目前所有的 screen 作业，并删除已经无法使用的 screen 作业。

在 screen 终端 下 按下 `Ctrl+a d` 键 可以离开 screen 作业

---

## 软件

### 命令行安装

如下三个命令是 Linux 系统中比较常见的用于安装软件的命令:

- **`apt-get`**：传统工具，稳定且功能强大，适合高级用户和脚本。
- **`apt`**：现代化的工具，推荐日常使用，特别是对于普通用户。
- **`aptitude`**：提供更高级的功能和图形界面，适合复杂包管理操作和依赖解决。

---

`apt-get`

- **简介**：`apt-get` 是较老的命令行工具，用于处理软件包的安装、更新和删除。
- **功能**：功能强大且稳定，是很多高级用户和脚本中的首选。
- **用户体验**：输出较为简洁，默认情况下没有启用进度条和颜色。
- 常用命令
  - `apt-get update`：更新包列表。
  - `apt-get upgrade`：升级已安装的包。
  - `apt-get install <package>`：安装包。
  - `apt-get remove <package>`：卸载包。
  - `apt-get dist-upgrade`：智能升级系统，处理依赖关系的变化。
  - `apt-get autoremove`：自动移除不再需要的包。

- `apt`
  - **简介**：`apt` 是 `apt-get` 和其他 `apt-*` 工具（如 `apt-cache`）的统一前端，旨在简化和统一命令行包管理。
  - **功能**：结合了 `apt-get` 和 `apt-cache` 的功能，提供简洁的命令。
  - **用户体验**：更人性化，输出更友好，默认启用一些高级功能（如进度条和颜色）。
  - 常用命令
    - `apt update`：更新包列表。
    - `apt upgrade`：升级已安装的包。
    - `apt install <package>`：安装包。
    - `apt remove <package>`：卸载包。
    - `apt search <package>`：搜索包。
    - `apt list`：列出包。
- `aptitude`
  - **简介**：`aptitude` 是 `apt-get` 的高级替代品，提供了图形界面和命令行界面，用于软件包管理。
  - **功能**：提供更高级的功能和更好的依赖管理，允许用户选择不同的解决方案来处理依赖冲突。
  - **用户体验**：交互式的图形界面（在终端内运行），适合需要更细致控制的用户。
  - 常用命令
    - `aptitude update`：更新包列表。
    - `aptitude upgrade`：升级已安装的包。
    - `aptitude install <package>`：安装包。
    - `aptitude remove <package>`：卸载包。
    - `aptitude search <package>`：搜索包。
    - `aptitude safe-upgrade`：安全升级包，避免删除或安装新的包。

---

### 查看软件安装位置

> [Ubuntu 中查看软件的安装位置及安装文件 - Macrored - 博客园 (cnblogs.com)](https://www.cnblogs.com/macrored/p/11757888.html)

```bash
whereis
which
```


---

### 软件包

类似于 Windows 上的 exe, msi, zip,  Linux 上得到软件包分发也有几种常见格式

- `tar`

  - `简介`: tar` 包（通常扩展名为 `.tar.gz` 或 `.tar.bz2`）是一种压缩归档文件，类似于 Windows 的 ZIP 文件。

    它通常用于分发源代码或其他文件集合。

  - 使用方法

    - 解压缩文件

      ```bash
      tar -xzvf filename.tar.gz  # 使用 gzip 压缩的 tar 包
      tar -xjvf filename.tar.bz2  # 使用 bzip2 压缩的 tar 包
      ```

    - 通常需要进入解压后的目录并根据包含的 `README` 或 `INSTALL` 文件的指示进行安装。一般情况下，使用以下命令：

      ```bash
      ./configure
      make
      sudo make install
      ```

- `deb`

  - `特点`: `deb` 包是 Debian 系列（包括 Ubuntu 等）的包管理格式。

    这种包通常包含二进制文件和依赖信息，安装时会自动解决依赖关系。

  - 使用方法

    安装 deb 包

    ```bash
    sudo dpkg -i filename.deb
    ```

    如果有依赖问题，可以使用以下命令来解决

    ```bash
    sudo apt-get install -f
    ```

- `AppImage` 

  - `特点`: `AppImage` 是一种便携的 Linux 应用程序格式。它包含应用程序的所有依赖项，可以在大多数现代 Linux 发行版上运行，而无需安装。

  - `使用方法`:

    - 赋予可执行权限

      ```bash
      chmod +x filename.AppImage
      ```

    - 运行应用程序

      ```bash
      ./filename.AppImage
      ```

---

### Firefox

> [在 Linux 中安装 Firefox | Firefox 帮助 (mozilla.org)](https://support.mozilla.org/zh-CN/kb/linux-firefox#w_cong-fa-xing-ban-ti-gong-de-bao-an-zhuang-tui-jian)
>
> ---

1. 从 [Firefox 下载页面](https://www.mozilla.org/firefox/linux/?utm_medium=referral&utm_source=support.mozilla.org) 并点击 {button 立即下载} 按钮。

2. 打开一个**终端**，转到下载 Firefox 的目录，比如

- `cd ~/Downloads`

3. 将下载文件的内容解压缩：

- `tar xjf firefox-\*.tar.bz2`

以下命令必须以 root 身份执行，或以 `sudo` 开头。

4. 将解压的 Firefox 目录移到 _/opt_:

- `mv firefox /opt`

5. 创建一个指向 Firefox 可执行文件的 symlink:

- `ln -s /opt/firefox/firefox /usr/local/bin/firefox`

6. 下载一个 desktop 文件：

- `wget https://ghproxy.com/https://raw.githubusercontent.com/mozilla/sumo-kb/main/install-firefox-linux/firefox.desktop -P /usr/local/share/applications`

如果，没有安装 `wget`，那么你可以右击以上链接，打开弹出菜单并选择 另存为。下载好文件之后，把它放到 _/usr/local/share/applications_。

你可以打开 [排障信息](https://support.mozilla.org/zh-CN/kb/使用故障排除信息页面来帮助解决Firefox的问题) 页面来验证安装是否成功。在 _应用基础_ 部分，Application Binary 应该是 `/opt/firefox/firefox-bin`。

---

### 微信

> [Ubuntu 下如何使用微信 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/413646220)
>
> [Ubuntu 安装微信，三步到位\_辞与不羡的博客-CSDN 博客\_ubuntu 安装微信](https://blog.csdn.net/m0_50502579/article/details/126096484)
>
> ---

安装 kylin.wine 封装版的微信

与 deepin 一样，ubuntukylin(优麒麟) 系统也第三方封装的 ubuntu。

```bash
# 下载Wine环境包：
wget http://archive.ubuntukylin.com/software/pool/partner/ukylin-wine_70.6.3.25_amd64.deb
# 下载微信(wine) 包：
wget http://archive.ubuntukylin.com/software/pool/partner/ukylin-wechat_3.0.0_amd64.deb
# 安装
sudo apt-get install -f -y ./ukylin-wine_70.6.3.25_amd64.deb
sudo apt-get install -f -y ./ukylin-wechat_3.0.0_amd64.deb
```

然后就可以在应用程序页面最后看到微信的图标了

---

## 常见问题

### the root filesystem require a manual fsck

> [boot - Root file system requires manual fsck - Ask Ubuntu](https://askubuntu.com/questions/885062/root-file-system-requires-manual-fsck)

![image-20220810092901637](http://cdn.ayusummer233.top/img/202208100929766.png)

```bash
fask -tf /dev/mapper/ubuntu--vg-root
exit
```

> [Linux fsck 命令 command not found fsck 未找到命令 fsck 命令详解 fsck 命令未找到 fsck 命令安装 - CommandNotFound ⚡️ 坑否](https://commandnotfound.cn/linux/1/451/fsck-命令)
>
> - `-y`: 确认所有的 yes/no 选项
> - `-f`: (force) 尽管目录被标记为 clean 也强制检查

---

### E: dpkg was interrupted, you must manually run 'dpkg --configure -a' to correct the problem.

执行 `dpkg --configure -a` 以修复

若执行后出现

```bash
dpkg: error: parsing file '/var/lib/dpkg/updates/0000' near line 0:
 newline in field name '▒v▒▒'
```

则

```bash
sudo rm /var/lib/dpkg/updates/*
```

即可

---

### E: Sub-process /usr/bin/dpkg returned an error code (1)

![image-20220825102350086](http://cdn.ayusummer233.top/img/202208251023257.png)

> [E: Sub-process /usr/bin/dpkg returned an error code (1)解决办法\_Mr.Stick 的博客-CSDN 博客](https://blog.csdn.net/stickmangod/article/details/85316142)

---

## game

### 手游相关

> [搭建 Reroid](https://b.hui.ke/posts/build-redroid/)  
> [remote-android/redroid-doc](https://github.com/remote-android/redroid-doc)

---

## Ubuntu 安装邮件服务器(TODO - 校验有问题且暂时不打算用, 已搁置)

> [Ubuntu 安装邮件服务器 - 简书 (jianshu.com)](https://www.jianshu.com/p/f438aa21069e)
>
> [The Postfix Home Page](http://www.postfix.org/)
>
> [在 Ubuntu 20.04 上配置 Postfix 以使用 Gmail SMTP-番茄网 (tomato.cm)](http://www.tomato.cm/1267.html)
>
> ---

Postifx 是 `Wietse Venema` 在 IBM 的\ GPL 协议之下开发的 `MTA`(邮件传输代理) 软件。是 Wietse Venema 想要为使用最广泛的 sendmail 提供替代品的一个尝试, 是一个 SMTP 服务器
